function operaciones(op) {
  fetch('../build/optimized.wasm')
    .then(response => response.arrayBuffer())
    .then(buffer => WebAssembly.instantiate(buffer))
    .then(module => {
      const exports = module.instance.exports;
      const addFunc = exports.add;
      const resFunc = exports.res;
      const mulFunc = exports.mul;
      const divFunc = exports.div;
      let operacion;
      let operando1;
      function name() {
        document.getElementById("resultado").value = 0
      }
      switch (op) {
        case 'sumar':
          operando1 = document.getElementById("resultado").value;
          document.getElementById("resultado").value = operando1 + "+";
          operacion = document.getElementById("resultado").value;
          document.getElementById("memoria").value = "suma";
          break;
        case 'restar':
          operando1 = document.getElementById("resultado").value;
          document.getElementById("resultado").value = operando1 + "-";
          operacion = document.getElementById("resultado").value;
          document.getElementById("memoria").value = "resta";
          break;
        case 'multiplicar':
          operando1 = document.getElementById("resultado").value;
          document.getElementById("resultado").value = operando1 + "*";
          operacion = document.getElementById("resultado").value;
          document.getElementById("memoria").value = "multiplicacion";
          break;
        case 'dividir':
          operando1 = document.getElementById("resultado").value;
          document.getElementById("resultado").value = operando1 + "/";
          operacion = document.getElementById("resultado").value;
          document.getElementById("memoria").value = "division";
          break;

        case 'Limpiar':
          const limpiar = document.getElementById('limpiar')
          document.getElementById("memoria").value = "limpiar";
          name()
          break;
        case 'igual':

          operacion = document.getElementById("resultado").value;
          const memoriaop = document.getElementById("memoria").value;

          let resultado;
          let operandos;

          switch (memoriaop) {
            case 'suma':
              operandos = operacion.split("+");
              resultado = addFunc(operandos[0], operandos[1]);
              document.getElementById("resultado").value = resultado;
              break;
            case 'resta':
              operandos = operacion.split("-");
              resultado = resFunc(operandos[0], operandos[1]);
              document.getElementById("resultado").value = resultado;
              break;
            case 'multiplicacion':
              operandos = operacion.split("*");
              resultado = mulFunc(operandos[0], operandos[1]);
              document.getElementById("resultado").value = resultado;
              break;
            case 'division':
              operandos = operacion.split("/");
              resultado = divFunc(operandos[0], operandos[1]);
              document.getElementById("resultado").value = resultado;
              break;
          }
          break;
      }
    })
}
