export function add(a: i32, b: i32): i32 {
  return a + b;
}

export function res(a: i32, b: i32): i32 {
  return a - b;
}

export function mul(a: i32, b: i32): i32 {
  return a * b;
}

export function div(a: i32, b: i32): i32 {
  return a / b;
}

